Feature: device association versioning

  HERE Tracking is a cloud product designed to address location tracking problems for a wide range of Location IoT industry verticals.
	
  Scenario: valid device version
    Given the device is available
    When I check the device version
    Then the version is "2.0.36-98cc32b"
    
  Scenario: invalid request method
    Given the device is available
    When I check the device version using a POST request
    Then the requested resource could not be found