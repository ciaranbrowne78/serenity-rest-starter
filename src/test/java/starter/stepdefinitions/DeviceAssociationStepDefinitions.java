package starter.stepdefinitions;

import static org.junit.Assert.assertEquals;
import static starter.status.DeviceHealth.HEALTHY;
import static net.serenitybdd.rest.SerenityRest.restAssuredThat;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Steps;
import starter.status.DeviceAssociationStatus;

public class DeviceAssociationStepDefinitions {

    @Steps
    DeviceAssociationStatus theDevice;

    @Given("the device is available")
    public void the_device_is_available() {
    	 assertEquals("The device is unavailable", HEALTHY, theDevice.currentStatus());
    }

    @When("I check the device version")
    public void i_check_the_device_version() {
    	theDevice.readVersionMessage();
    }

    @When("I check the device version using a POST request")
    public void i_check_the_device_version_using_a_POST() {
    	theDevice.readVersionUsingPOST();
    }
    
    @Then("the version is {string}")
    public void the_version_is(final String expectedVersion) {
    	assertEquals("Version is not as expected!", expectedVersion, SerenityRest.lastResponse().jsonPath().getJsonObject("device-associations"));
    }
    
    @Then("the requested resource could not be found")
    public void the_requested_resource_could_not_be_found() {
    	assertEquals("Status code is not as expected!", 404, SerenityRest.lastResponse().getStatusCode());
    	assertEquals("Response message not as expected!", "Not Found", SerenityRest.lastResponse().jsonPath().getJsonObject("message"));
    	restAssuredThat(lastResponse -> lastResponse.statusCode(404).extract().jsonPath().getString("error").equals("Not Found"));
    }
}
