package starter.status;

import static starter.DeviceWebServiceEndPoints.HEALTH;
import static starter.DeviceWebServiceEndPoints.VERSION;

import io.restassured.RestAssured;
import net.serenitybdd.rest.SerenityRest;

public class DeviceAssociationStatus {

    public DeviceHealth currentStatus() {
        return (RestAssured.get(HEALTH.getUrl()).statusCode() == 200) ? DeviceHealth.HEALTHY : DeviceHealth.DOWN;
    }

    public void readVersionMessage() {
        SerenityRest.given()
        			.accept("application/json")
        			.when()
        			.get(VERSION.getUrl());
    }

	public void readVersionUsingPOST() {
		SerenityRest.post(VERSION.getUrl());
	}
}
