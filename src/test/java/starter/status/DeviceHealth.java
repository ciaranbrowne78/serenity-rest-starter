package starter.status;

public enum DeviceHealth {
    HEALTHY, DOWN
}
