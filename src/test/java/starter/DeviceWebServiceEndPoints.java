package starter;

public enum DeviceWebServiceEndPoints {
    HEALTH("https://tracking.api.here.com/device-associations/v2/health"),
    VERSION("https://tracking.api.here.com/device-associations/v2/version");

    private final String url;

    DeviceWebServiceEndPoints(String url) {
        this.url = url;
    }

    public String getUrl() {
        return url;
    }
}
