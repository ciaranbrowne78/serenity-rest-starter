# LeasePlan BE QA AUTO TEST ASSIGNMENT

## Introduction

These are instructions on how to install, run and write Serenity Cucumber Rest API tests.

The sample endpoints used are taken from [HERE Tracking](https://any-api.com/here_com/tracking/docs/API_Description) on any-api.com.
_HERE Tracking is a cloud product designed to address location tracking problems for a wide range of Location IoT industry verticals._

## The starter project

The solution is based on the forked [serenity-rest-starter](https://github.com/serenity-bdd/serenity-rest-starter) project in Github. 
This starter project was then imported into the open DEVOPs platform [Gitlab](https://gitlab.com/ciaranbrowne78/serenity-rest-starter).

This starter project gives you a basic project setup, along with some sample tests and supporting classes and comes bundled with a sample SpringBoot web service, and some RestAssured-based tests. Please note all sample Springboot files were removed. 


## Framework & Design Considerations
- Serenity BDD is a library that makes it easier to write high quality automated acceptance tests, with powerful reporting and living documentation features 
- It has strong support for not just API testing using RestAssured, but also for web testing with Selenium
- API calls & validations are made using RestAssured and SerenityRest, which is a wrapper on top of RestAssured 
- Tests are written in BDD Gherkin format in Cucumber feature files and it is represented as a living documentation in the test report 

### The project directory structure

```Gherkin
src
  + test
    + java                          Test runners and supporting code
      + starter                     Domain model package consisting of all actions/questions on HERE device association functionality
          CucumberTestSuite         Test	runner
          DeviceWebServiceEndpoints HERE tracking endpoints
      + starter.status              Package for all common actions and questions
          DeviceAssociationStatus   Action class containing questions/validations
          DeviceHealth              device association status
      + starter.stepdefinitions     Step definitions for the BDD feature
    + resources
      + features                    Feature files directory
      Serenity.conf                 Configurations file
```

## Executing the tests
Automated tests are executed on a working CI/CD pipeline in Gitlab.

Test reports can be seen for each job in Gitlab, under the *Job artifacts* section and by navigating to _target/site/serenity/index.html_. 
The report records the API calls and their responses in a very readable format.
Each step in the tests are clearly documented for readability and debugging in case of failures.


## Scenario's
Both scenario's defined within the feature file, make 2 GET API calls:
1. First call is made to the _/health_ endpoint which returns whether the service is available or not
2. Second call is made to the _version_ endpoint to confirm the device version

There are two scenario's defined within the feature file:
1. A positive test case where the device is available and the correct version is returned

```Gherkin
  Scenario: valid device version
    Given the device is available
    When I check the device version
    Then the version is "2.0.36-98cc32b"
```
2. A negative test case where the device is available but an incorrect http request method is used i.e. POST

```Gherkin    
  Scenario: invalid request method
    Given the device is available
    When I check the device version using a POST request
    Then the requested resource could not be found
```

## Steps
The glue code for these scenario's illustrate the layered approach that works well for API acceptance tests.

The glue code below is responsible for orchestrating calls to a layer of more business-focused classes, which perform the actual REST calls.
In the steps that perform assertions, we can also use _SerenityRest.restAssuredThat()_, which lets us make a RestAssured assertion on the last response the server sent us.

```java
    @Steps
    DeviceAssociationStatus theDevice;

    @Given("the device is available")
    public void the_device_is_available() {
    	 assertEquals("The device is unavailable", HEALTHY, theDevice.currentStatus());
    }

    @When("I check the device version")
    public void i_check_the_device_version() {
    	theDevice.readVersionMessage();
    }

    @When("I check the device version using a POST request")
    public void i_check_the_device_version_using_a_POST() {
    	theDevice.readVersionUsingPOST();
    }
    
    @Then("the version is {string}")
    public void the_version_is(final String expectedVersion) {
    	assertEquals("Version is not as expected!", expectedVersion, SerenityRest.lastResponse().jsonPath().getJsonObject("device-associations"));
    }
    
    @Then("the requested resource could not be found")
    public void the_requested_resource_could_not_be_found() {
    	assertEquals("Status code is not as expected!", 404, SerenityRest.lastResponse().getStatusCode());
    	assertEquals("Response message not as expected!", "Not Found", SerenityRest.lastResponse().jsonPath().getJsonObject("message"));
    	restAssuredThat(lastResponse -> lastResponse.statusCode(404).extract().jsonPath().getString("error").equals("Not Found"));
    }
```

The actual REST calls are performed using RestAssured and SerenityRest in the action classes, like _DeviceAssociationStatus_:

```java
public class DeviceAssociationStatus {

    public DeviceHealth currentStatus() {
        return (RestAssured.get(HEALTH.getUrl()).statusCode() == 200) ? DeviceHealth.HEALTHY : DeviceHealth.DOWN;
    }

    public void readVersionMessage() {
        SerenityRest.given()
        			.accept("application/json")
        			.when()
        			.get(VERSION.getUrl());
    }

	public void readVersionUsingPOST() {
		SerenityRest.post(VERSION.getUrl());
	}
}
```
If you don't want the queries to appear in the reports use _RestAssured_ otherwise use _SerenityRest_.
